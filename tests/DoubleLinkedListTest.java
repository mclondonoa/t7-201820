

import java.util.Comparator;
import org.junit.Assert;
import java.time.LocalDateTime;
import javafx.util.converter.LocalDateStringConverter;

import org.junit.*;
import static org.junit.Assert.*;
import model.data_structures.DoublyLinkedList;
import model.vo.Trip;

public class DoubleLinkedListTest {

	

	@Test
	<T> void testAdd( ) {
		try {
			
			DoublyLinkedList n =  new DoublyLinkedList();
			T i = (T) "Male";
			T x = (T) "";
			
			
			
			n.add( (Comparable) x);
			T obj = (T) n.remove((Comparable) i);
			assertNull(obj);
			
		} catch (Exception e) {
			// TODO: handle exception
			
		}
		
		
	}

	@Test
	<T> void testDelete() {
		try {

			DoublyLinkedList n =  new DoublyLinkedList();
			Trip i = new Trip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,69,"Male");
			n.remove(i);
			fail("Se espera Exception");

		} catch (Exception e) {
			// TODO: handle exception

		}
	}
	
	@Test
	<T> void testGetCurrentElement() {
		
		try {
			DoublyLinkedList n =  new DoublyLinkedList();
			T element = (T) n.get(0);
			assertNull(element);
		}
		
		catch(Exception e)
		{
			
		}
		
	}
	
	@Test
	<T> void testAddFirst() {
		try {
			DoublyLinkedList n =  new DoublyLinkedList();
			Trip i = new Trip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,69,"Male");
			n.addFirst(i);
			T element = (T) n.get(0);
			assertNotNull(element);

		}
		catch (Exception e) {
			
		}
	}
	
	@Test
	void testSize() {
		try {
		DoublyLinkedList n =  new DoublyLinkedList();
		Trip i = new Trip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,69,"Male");			
		int size =n.size();
		assertTrue(size == 1);
	}
		catch(Exception e) {
			fail("El tama�o debe ser 1");
		}
	}
	
	@Test
	<T> void testGet() {
		try {
			DoublyLinkedList n =  new DoublyLinkedList();
			Trip i = new Trip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,69,"Male");			
			T k = (T) n.get(i);			
			assertNull(k);
			
		}
		catch (Exception e) {

		}


	}
	
	@Test
	<T> void testDeleteFirst() {
		
		
		try {
			DoublyLinkedList n =  new DoublyLinkedList();
			T answer = (T) n.deleteFirst();
			assertNull(answer);
			
		}
		catch (Exception e) {
			
		}
	}

}



import java.util.Random;
import model.data_structures.HashLinearProbing;
import model.data_structures.SeparateChainingHashST;
import org.junit.Assert;
import org.junit.*;
import static org.junit.Assert.*;
public class HashTablesTest {
	@Test
	public void testSeparateChaining(){
		SeparateChainingHashST<Integer, String> sc = new SeparateChainingHashST<Integer, String>(5);
		int[] p = new  int[1000000];
		Random y = new Random();
		for (int i = 0; i < p.length; i++) {
			int n = y.nextInt(1000000000);
			p[i]=n;
		}
		long tiempoInicial = System.currentTimeMillis();
		for (int i = 0; i < p.length; i++) {
			sc.put(p[i ], p[i] + "");
		}
		long tiempoFinal=System.currentTimeMillis();
		System.out.println("Tiempo de carga separate chaining: " + (tiempoFinal-tiempoInicial));
		
		String a; 
		y = new Random();
		tiempoInicial = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			int n = y.nextInt(1000000);
			a = (String) sc.get(p[n]);

		}
		tiempoFinal= System.currentTimeMillis();
		System.out.println("Tiempo separate chaining: " + (tiempoFinal-tiempoInicial));
		
		
		System.out.println("Tamano separate chaining: " + sc.size());
		
		System.out.println("Factor de carga separate chaining: " + (sc.factorCarga()));
	}
	@Test
	public void LineaProbingGetTest() {

			
			HashLinearProbing<Integer, String> nuevo = new HashLinearProbing<Integer, String>(50000);
			int[] prueba = new  int[1000000];
			Random x = new Random();
			for (int i = 0; i < prueba.length; i++) {
				int n = x.nextInt(1000000000);
				prueba[i]=n;
			}
			long timeInicial = System.currentTimeMillis();
			for (int i = 0; i < prueba.length; i++) {
				nuevo.put(prueba[i ], prueba[i] + "");
			}
			long timeFinal=System.currentTimeMillis();
			System.out.println("Tiempo de carga" + (timeFinal-timeInicial));
			
			String nat; 
			x = new Random();
			timeInicial = System.currentTimeMillis();
			for (int i = 0; i < 10000; i++) {
				int n = x.nextInt(1000000);
				nat = (String) nuevo.get(prueba[n]);
				assertTrue("Mensaje no es " , nat.equals(prueba[n] + " "));
				
			}
			timeFinal= System.currentTimeMillis();
			System.out.println("Tiempo " + (timeFinal-timeInicial));
			
			
			System.out.println("Tamano " + nuevo.getM());
			
			System.out.println("Factor de carga " + (nuevo.getN()/nuevo.getM()));
		
		
		
	}
	
}

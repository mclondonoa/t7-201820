

import java.util.Iterator;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.RedBlackTree;
import org.junit.Assert;
import org.junit.*;
import static org.junit.Assert.*;


public class RedBlackBSTTest 
{

    private RedBlackTree<Integer, String> redBlackBST;

    
    private int size;
    
    private void setupEscenario1( )
    {
    	
        redBlackBST = new RedBlackTree<>();
        size = 0;

    }

    private void setupEscenario2( )
    {
    	
        redBlackBST = new RedBlackTree<>();
        size = 10;
        for( int cont = 0; cont < size; cont++ )
        {
            redBlackBST.put(cont, "Test " + cont);
        }
    }


    private void setupEscenario3( )
    {
    	
        redBlackBST = new RedBlackTree<>();
        size = 101;
        for( int cont = size; cont > 0; cont--)
        {
            redBlackBST.put(cont, "Test " + cont);
        }
    }

    @Test
    public void testPut( )
    {
        setupEscenario1( );
        size = 11;
        for( int cont = 0; cont < size; cont++ )
        {
            redBlackBST.put(cont, "Test " + cont);
        }
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(0), "Test 0" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(3), "Test 3" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(7), "Test 7" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(10), "Test 10" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(20), null );
        redBlackBST.put(0, "Test " + 54);
        redBlackBST.put(0, "Test " + 32);
        redBlackBST.put(0, "Test " + 10);
        redBlackBST.put(100, "Test " + 100);
        size ++;
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(0), "Test 10" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(100), "Test 100" );
        assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );
    
    }
    
    @Test
    public void testDelete( )
    {
    	setupEscenario1( );
        size = 11;
        for( int cont = 0; cont < size; cont++ )
        {
            redBlackBST.put(cont, "Test " + cont);
        }
        

        redBlackBST.delete(4);
        redBlackBST.delete(7);
        
        assertEquals( "No se borro correctamente", redBlackBST.get(4), null );
        assertEquals( "No se borro correctamente", redBlackBST.get(7), null );
        
        redBlackBST.delete(4);
        redBlackBST.delete(10);
        redBlackBST.delete(2);
        redBlackBST.delete(1);
        
        assertEquals( "No se borro correctamente", redBlackBST.get(4), null );
        assertEquals( "No se borro correctamente", redBlackBST.get(10), null );
        assertEquals( "El tamaño no es el correcto", size-5, redBlackBST.size( ) );
        

    }

    @Test
    public void testGet( )
    {
    	setupEscenario2( );
        
    	for( int cont = 0; cont < size; cont++ )
        {
    		assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(cont), "Test "+ cont);
        	
        }
        
            // Verifica que la informacion sea correcta
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(4), "Test 4");
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(0), "Test 0");
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(8), "Test 8");
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(2), "Test 2");
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(10), null);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.get(50), null);
            

        	redBlackBST.put(0, "Test " + 54);
            redBlackBST.put(7, "Test " + 54);
            
            assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(0), "Test 54" );
            assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(0), "Test 54" );
            
            assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );
         

   
    }
    

    @Test
    public void testSize( )
    {
        setupEscenario3( );
        
        assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );
      
        
        redBlackBST.put( 50, "Test 74");
        
        assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );
     
     
        redBlackBST.delete(54);
        redBlackBST.delete(64);
        redBlackBST.delete(100);
        size=size-3;
        assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );
      
    }

    @Test
    public void testEstructura( )
    {
    	
    	setupEscenario1();
    	size = 20; 
        for( int cont = 0; cont < size; cont++ )
        {
            redBlackBST.put(cont, "Test " + cont);
        }
        
        redBlackBST.put(30, "Test " + 54);
        redBlackBST.put(50, "Test " + 32);
        redBlackBST.put(70, "Test " + 10);
        redBlackBST.put(100, "Test " + 100);

        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(30), "Test 54" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(70), "Test 10" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(100), "Test 100" );
        assertEquals( "La adicion no se realizo correctamente", redBlackBST.get(101), null );

        size = size+4;

        assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );
        
        
    }
    
    @Test
    public void testKeys( )
    {
        setupEscenario2( );
        
        Queue<Integer> listaLlaves=  (Queue<Integer>) redBlackBST.keys();
        
       Integer i=0;
       Integer current= listaLlaves.dequeue();
		while(current!=null) {
			assertEquals( "La información de los elementos esta mal", current, i);
			i++;
			current= listaLlaves.dequeue();
		}	

        assertEquals( "El tamaño de la lista no es el correcto", size, redBlackBST.size( ) );
     
    }
    
    @Test
    public void testContains( )
    {
    	setupEscenario3( );

        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(4), true);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(8), true);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(54), true);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(3), true);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(2), true);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(97), true);
        	
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(150), false);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(200), false);
        	assertEquals(  "No se obtuvo el elemento indicado", redBlackBST.contains(300), false);

            assertEquals( "El tamaño no es el correcto", size, redBlackBST.size( ) );

    }

    @Test
    public void testIsEmpty( )
    {
    	setupEscenario1( );
        assertEquals( "Debe estar vacia.", redBlackBST.isEmpty(), true );
         
            size = 11;
            for( int cont = 0; cont < size; cont++ )
            {
                redBlackBST.put(cont, "Test " + cont);
            }
         assertEquals( "No debe estar vacia.", redBlackBST.isEmpty(), false );
              
   
    }
    
    @Test
    public void testMin( )
    {
    	setupEscenario3( );

        assertEquals( "Ese no es el minimo.", 1 , (int) redBlackBST.min() );
         
        redBlackBST.delete(1);
        redBlackBST.delete(2);
        redBlackBST.delete(3);
                
        assertEquals( "Ese no es el minimo.", 4 , (int) redBlackBST.min() );
        
    }

    @Test
    public void testMax( )
    {
    	setupEscenario3( );

        assertEquals( "Ese no es el maximo.", 101 , (int) redBlackBST.max() );
         
        redBlackBST.deleteMax();
        redBlackBST.delete(100);
        redBlackBST.deleteMax();
                
        assertEquals( "Ese no es el maximo.", 98 , (int) redBlackBST.max() );
        
    }
}
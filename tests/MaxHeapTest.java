import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.data_structures.MaxHeapCP;

public class MaxHeapTest {

	@Test
	public <T> void maxTest() {
		MaxHeapCP nuevo = new MaxHeapCP(50);
		T elemento = (T) nuevo.max();
		assertNull(elemento);
	}

	@Test
	public <T> void agregarTest() {

		try {
			MaxHeapCP nuevo = new MaxHeapCP(1);
			T elemento = (T) "Hola";
			T elemento2= (T) "Nat";
			nuevo.agregar((Comparable) elemento);
			nuevo.agregar((Comparable) elemento2);
			fail("Debi� lanzar excepci�n");

		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}

	@Test
	public void darNumElem() {
		MaxHeapCP nuevo = new MaxHeapCP(50);
		assertTrue(nuevo.darNumElementos() == 0);
	}
}

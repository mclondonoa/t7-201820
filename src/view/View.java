package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Scanner;

import controller.Controller;
import controller.Controller.ResultadoCampanna;
import model.data_structures.ICola;
import model.data_structures.IHash;
import model.data_structures.ILista;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;

public class View {

	public static void main(String[] args){

		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		Controller controlador = new Controller();
		int option;
		int n;
		int limiteSuperior;
		int limiteInferior;
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();
			option = linea.nextInt();
			n = 0;
			limiteInferior =0;
			limiteSuperior =0;
			switch(option)
			{

			case 0:  //Carga de datos
				String dataTrips = "";  // ruta del archivo de Trips
				String dataStations = ""; // ruta del archivo de Stations
				String dataBikeRoutes = ""; // ruta del archivo de ciclorutas
				dataTrips = Manager.TRIPS_Q1 + ":" + Manager.TRIPS_Q2 +":" + Manager.TRIPS_Q3 + ":" + Manager.TRIPS_Q4;
				dataStations = Manager.STATIONS_Q1_Q2 + ":" + Manager.STATIONS_Q3_Q4;
				dataBikeRoutes = Manager.BIKE_ROUTES;
				
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				Controller.cargarDatos(dataTrips, dataStations, dataBikeRoutes);
				
				//TODO
				System.out.println("Total trips cargados en el sistema: ï¿½XXX?");
				//TODO
				System.out.println("Total estaciones cargadas en el sistema: ï¿½YYY?");
				//TODO
				System.out.println("Total ciclorutas cargadas en el sistema: ï¿½ZZZ?");
				
				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;


			case 1: //Req A1

				//Capacidad
				System.out.println("Ingrese la capacidad de la estaciï¿½n: (Ej: 56)");
				String capacidad = linea.next();
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1A = linea.next();


				// Datos Fecha inicial
				LocalDate localDateInicio = convertirFecha(fechaInicialReq1A);

				//Metodo 1A
				ICola<Trip> viajesA1 = controlador.A1(Integer.parseInt(capacidad), localDateInicio);
				for(Trip v : viajesA1)
				{
					//Aquï¿½ debe imprimir lo que indica el enunciado
				}
				break;

			case 2: //Req A2
				
				System.out.println("Ingrese la duraciï¿½n deseada:");
				//Duraciï¿½n de los viajes
				  n = Integer.parseInt(linea.next());
				
				ILista<Trip> viajesA2 = controlador.A2(n);
				
				for(Trip v : viajesA2)
				{
					//Aquï¿½ debe imprimir lo que indica el enunciado
				}
				break;

			case 3: //Req A3
				
				//Nï¿½mero de viajes que se desan buscar
				System.out.println("Ingrese el nï¿½mero de viajes a buscar: ");
				n = Integer.parseInt(linea.next());


				//Fecha
				System.out.println("Ingrese la fecha (Ej : 28/3/2017)");
				String fechaInicialReq3A = linea.next();

				// Datos Fecha
				LocalDate localDateInicio3A = convertirFecha(fechaInicialReq3A);

				//Metodo A3
				ILista<Trip> viajesPorBicicleta = controlador.A3(n, localDateInicio3A);
				for(Trip v : viajesPorBicicleta)
				{
					//Aquï¿½ debe imprimir lo que indica el enunciado
				}
				break;

			case 4: //Req B1
				
				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo minimo total recorrido: ");
				limiteInferior = linea.nextInt();
				
				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo mï¿½ximo total recorrido: ");
				limiteSuperior = linea.nextInt();
				

				//Metodo B1
				ILista<Bike> bikesB1 = controlador.B1(limiteInferior, limiteSuperior);
				
				for(Bike v : bikesB1)
				{	
					//Aquï¿½ debe imprimir lo que indica el enunciado
				}
				break;

			case 5: //Req B2
				
				//Fecha Inicial
				System.out.println("Ingrese la fecha inicial (Ej : 28/3/2017)");
				String fechaInicialReq1B = linea.next();

				//Hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00)");
				String horaInicialReq1B = linea.next();

				// Datos Fecha/Hora inicial
				LocalDateTime localDateInicio1B = convertirFecha_Hora_LDT(fechaInicialReq1B, horaInicialReq1B);

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 29/3/2017)");
				String fechaFinalReq2B = linea.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 14:25:30)");
				String horaFinalReq2B = linea.next();

				// Datos Fecha/Hora final
				LocalDateTime localDateFin2B = convertirFecha_Hora_LDT(fechaFinalReq2B, horaFinalReq2B);

				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo minimo (en segundos) del recorrido: ");
				limiteInferior = linea.nextInt();
				
				//Tiempo minimo que debe recorrer la bicicleta para entrar en el rango
				System.out.println("Ingrese el tiempo mï¿½ximo (en segundos) del recorrido: ");
				limiteSuperior = linea.nextInt();
				
				
				//Req B2
				ILista<Trip> viajesB2 = controlador.B2(localDateInicio1B, localDateFin2B, limiteInferior, limiteSuperior);
				
				for(Trip v : viajesB2)
				{
					//Aquï¿½ debe imprimir lo que indica el enunciado
				}
				break;

				
			case 6: //Req B3
				//Estacion de inicio
				System.out.println("Ingrese la estaciÃ³n de inicio (Ej : Shedd Aquarium)");
				String estacionDeInicio = linea.next();
				
				//Estacion de llegada
				System.out.println("Ingrese la estaciÃ³n de llegada (Ej : Shedd Aquarium)");
				String estacionDeLlegada = linea.next();

				int [] resultados = controlador.B3(estacionDeInicio, estacionDeLlegada);
				int horaEnteraConMayorNumeroDeViajes = resultados[0];
				int totalViajes = resultados[1];
				
				//TODO Imprimir con un mejor formato
				System.out.println("Hora:" + horaEnteraConMayorNumeroDeViajes);
				System.out.println("totalViajes:" + totalViajes);

				break;
				
			case 7: //Req C1
				
				System.out.println("Ingrese cuanto estaria dispuesto a pagar por punto (Ej: 3.5):");
				double valorPorPunto = Double.parseDouble(linea.next());
				
				System.out.println("Ingrese el nÃºmero de estaciones donde se quiere poner publicidad (Ej: 4):");
				int numEstacionesConPublicidad = Integer.parseInt(linea.next());
				
				System.out.println("Ingrese un mes entre Febrero y Noviembre (Ej: 3):");
				int mesCampanna = Integer.parseInt(linea.next());
				
				ResultadoCampanna res = Controller.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);

				System.out.println("Total a pagar: " + res.costoTotal);
				System.out.println("Estaciones");

				for(Station t : res.estaciones)
				{
					System.out.print(t.getStationName());
				}
				break;
		
			case 8: //Req C2
				
				System.out.println("Ingrese el numero de divisiones en latitud (LA)");
				int LA = Integer.parseInt(linea.next());
				
				System.out.println("Ingrese el numero de divisiones en longitud (LO)");
				int LO = Integer.parseInt(linea.next());
				
				double [] rectangulo = controlador.C2(LA, LO);
				System.out.println("latitud maÌ�xima: " + rectangulo[0]);
				System.out.println("longitud maÌ�xima: " + rectangulo[1]);
				System.out.println("latitud miÌ�nima: " + rectangulo[2]);
				System.out.println("longitud miÌ�nima: " + rectangulo[3]);
				break;
			
			case 9: //Req C3
				System.out.println("Ingrese una latitud: (74.33)");
				double latitud = Double.parseDouble(linea.next());

				System.out.println("Ingrese una longitud: (-4.33)");
				double longitud = Double.parseDouble(linea.next());
				
				int sector = controlador.darSector(latitud, longitud);
				System.out.println("Sector: " + sector);
				
				System.out.println("Estaciones: ");
				ILista<Station> estacionesCercanas = controlador.C3(latitud, longitud);
				for(Station s: estacionesCercanas) {
					System.out.print("id: " + s.getStationId() + ", " );
					System.out.print("nombre:" + s.getStationName() + ", ");

					//TODO imprimir localizacion y distancia en metros a la posicion de entrada
				}
				break;
				
				
			case 10: //Req C4
				System.out.println("Ingrese una latitud: (74.33)");
				double latitudR = Double.parseDouble(linea.next());

				System.out.println("Ingrese una longitud: (-4.33)");
				double longitudR = Double.parseDouble(linea.next());
				
				int sectorR = controlador.darSector(latitudR, longitudR);
				System.out.println("Sector: " + sectorR);
				
				System.out.println("Ciclorutas: ");
				ILista<BikeRoute> ciclorutasCercanas = controlador.C4(latitudR, longitudR);
				for(BikeRoute b: ciclorutasCercanas) {
					System.out.print("Calle de referencia:" + b.getCalleReferencia() + ", ");
					//TODO imprimir los otros atributos que se piden en el enunciado
				}
				break;
			
			case 11: //Req C5
				System.out.println("Ingrese una latitud inicial: (74.33)");
				double latitudI = Double.parseDouble(linea.next());

				System.out.println("Ingrese una longitud inicial: (-4.33)");
				double longitudI = Double.parseDouble(linea.next());
				
				System.out.println("Ingrese una latitud final: (74.33)");
				double latitudF = Double.parseDouble(linea.next());

				System.out.println("Ingrese una longitud final: (-4.33)");
				double longitudF = Double.parseDouble(linea.next());
				
				System.out.println("Ciclorutas: ");
				ILista<BikeRoute> ciclorutasQueSePuedenUsar = controlador.C5(latitudI, longitudI, latitudF, longitudF);
				for(BikeRoute b: ciclorutasQueSePuedenUsar) {
					System.out.print("Calle de referencia:" + b.getCalleReferencia() + ", ");
					//TODO imprimir los otros atributos que se piden en el enunciado
				}
				break;
				
			case 12: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("-----------------ISIS 1206 - Estructuras de Datos------======----");
		System.out.println("-------------------- Proyecto 2   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("0. Cargar datos de todos los archivos");

		System.out.println("\nParte A:\n");
		System.out.println("1. Viajes que terminaron en una estaciï¿½n con cierta capacidad, en una fecha dada (1A)");
		System.out.println("2. Viajes con duraciones similares (2A)");
		System.out.println("3. Viajes con los recorridos mï¿½s largos en una fecha dada (3A)");

		System.out.println("\nParte B:\n");
		System.out.println("4. Bicicletas para mantenimiento (1B)");
		System.out.println("5. Viajes por estaciones de salida y llegada en un rango de tiempo (2B)");
		System.out.println("6. Hora pico de viajes por estaciones de salida y llegada (3B)");



		System.out.println("\nParte C:\n");
		System.out.println("7.  Campaï¿½a de publicidad (2C)");
		System.out.println("8.  Sectorizaciï¿½n (3C)");
		System.out.println("9. BuÌ�squeda georreferenciada (latitud y longitud) de estaciones");
		System.out.println("10. BuÌ�squeda georreferenciada (latitud y longitud) de ciclorutas");
		System.out.println("11. BuÌ�squeda de ciclorutas para hacer viaje");
		System.out.println("12. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		String[] datosFecha = fecha.split("/");
		

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);

		return LocalDate.of(agno, mes, dia);
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
	

}

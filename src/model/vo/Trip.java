package model.vo;

import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";

    private int tripId;
    private String startTime;
    private String stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private int endStationId;
    private String gender;

    public Trip(int tripId, String startTime, String stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.gender = gender;
    }

    @Override
    public int compareTo(Trip o) {
    	// TODO completar
        return 0;
    }

    public int getTripId() {
        return tripId;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }
}

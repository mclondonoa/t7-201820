package model.logic;

import API.IManager;
import controller.Controller.ResultadoCampanna;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.RedBlackTree;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;

import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "Ruta Trips 2017-Q1 en directorio data";
	
	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "Ruta Trips 2017-Q2 en directorio data";
		
	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "Ruta Trips 2017-Q3 en directorio data";
			
	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "Ruta Trips 2017-Q4 en directorio data";
		
	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "Ruta Stations 2017-Q1-Q2 en directorio data";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "Ruta Stations 2017-Q3-Q4 en directorio data";
	
	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String BIKE_ROUTES = "data/bikes.json";
	
	//Carga de datos
	private RedBlackTree<String, BikeRoute> treeRutas; 
		
		
	@Override
	public void cargarDatos(String rutaTrips, String rutaStations, String dataBikeRoutes) {
		// TODO Auto-generated method stub
		
			File f = new File(dataBikeRoutes);
			org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
			treeRutas = new RedBlackTree<>();
			try{
				JSONArray arr = (JSONArray) parser.parse(new FileReader(dataBikeRoutes));
				for(Object o : arr){
					JSONObject cicloruta = (JSONObject) o;

					String bikeroute = null;
					bikeroute = (String) cicloruta.get("BIKEROUTE");

					String the_geom = null;
					the_geom = (String) cicloruta.get("the_geom");

					String street = null;
					street = (String) cicloruta.get("STREET");

					String fstreet = null;
					fstreet = (String) cicloruta.get("F_STREET");

					String tstreet = null;
					tstreet = (String) cicloruta.get("T_STREET");

					String shape = null;
					shape = (String) cicloruta.get("Shape_Leng");
					
					String id = null;
					id = (String) cicloruta.get("id");

					BikeRoute r = new BikeRoute(the_geom, bikeroute, street, fstreet, tstreet, Double.parseDouble(shape));
					treeRutas.put(id, r);
					
				}
				System.out.println(treeRutas.alturaProd());
				
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		}	
	
	@Override
	public ICola<Trip> A1(int n, LocalDate fechaTerminacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A2(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A3(int n, LocalDate fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		return new int[] {0, 0};
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int darSector(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ILista<Station> C3(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub
		return null;
	}

	
	}
	


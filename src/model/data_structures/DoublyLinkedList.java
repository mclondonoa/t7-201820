package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList <T extends Comparable <T>> implements IDoublyLinkedList<T>{
	protected Node<T> first;
	protected Node<T> last;
	protected int size;


	public DoublyLinkedList(){
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public void add(T elem){
		Node<T> node = new Node<T>(elem);
		if ( first == null )
		{
			first = last = node;
			size++;
		}
		else
		{
			last.setNext(node);
			node.setPrev(last);
			last = node;
			size++;
		}
	}


	@Override
	public void addFirst(T agregado){
		Node<T> node = new Node<T>(agregado);
		if(first==null)
		{
			first = node ;
			last = node;
			size++;
		}
		else
		{
			node.setNext(first);
			first.setPrev(node);
			first= node;
			size++;
		}
	}

	@Override
	public T remove(T t) {
		Node<T> current = first;
		Node<T> prev = current.prev();

		while ( current != null )
		{
			if ( current.get().equals(t) )
			{
				Node<T> next = current.next();
				if( prev != null )
				{
					prev.setNext( next );
				}
				else
				{
					first = next;
				}
				if ( next != null )
				{
					next.setPrev(prev);
				}
				if( current == last )
				{
					last = current.prev();
				}
				size--;
				return t;
			}
			prev = current;
			current = current.next();
		}
		return null;
	}

	@Override
	public Node<T> deleteFirst(){
		Node<T> answer = first;
		if(first!= null)
		{
			first = first.next();
			if(first!= null)
				first.setPrev(null);
			size--;
		}

		return answer;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public T get(T t) {
		Node<T> current = first;
		while ( current != null )
		{
			if ( current.get().equals(t) )
			{
				return t;
			}
			current = current.next();
		}
		return null;
	}




	@Override
	public T get(int pos) {
		Node<T> elem = first;
		for(int i = 0 ; i < pos ; i++) 
		{
			elem = elem.next();
		}
		return elem.get();
	}

	@Override
	public boolean isEmpty() {
		if( size == 0 || first==null)
			return true;
		else
			return false;
	}

	@Override
	public Iterator<T> iterator() {
		return first.iterator();
	}

	@Override
	public Node<T> getLast() {
		return last;
	}

	@Override
	public Node<T> getFirst() {

		return first;
	}


	

}

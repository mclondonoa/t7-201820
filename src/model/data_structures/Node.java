/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author user
 * @param <T>
 *
 */
public class Node<T extends Comparable <T>> implements Iterable< T> 
{
	// --------------------------------------------------------------------------------
	// Attributes
	// --------------------------------------------------------------------------------
	
	/**
	 * Elemento que se encuentra en el nodo
	 */
	public T elem;
	
	/**
	 * Nodo previo a this
	 */
	public Node<T> prev;
	
	/**
	 * Nodo posterior a this
	 */
	public Node<T> next;

	// --------------------------------------------------------------------------------
	// Constructor
	// --------------------------------------------------------------------------------

	/**
	 * Crea un nodo
	 * @param pElem Elemento que se encuentra en el nodo
	 */
	public Node(T pElem)
	{
		elem = pElem;
		prev = null;
		next = null;
	}

	// --------------------------------------------------------------------------------
	// Methods
	// --------------------------------------------------------------------------------

	/**
	 * Da el elemento que se encuentra en el nodo
	 * @return Elemento del nodo
	 */
	public T get() 
	{
		return elem;
	}

	/**
	 * Da el nodo siguiente
	 * @return Next node
	 */
	public Node<T> next() 
	{
		return next;
	}

	/**
	 * Define el nodo siguiente al actual
	 * @param node Nuevo nodo next
	 */
	public void setNext(Node<T> node) 
	{
		next = node;
	}

	/**
	 * Da el nodo anterior al actual
	 * @return Nodo prev
	 */
	public Node<T> prev() 
	{
		return prev;
	}

	/**
	 * Define el siguiente nodo
	 * @param node Nuevo nodo prev
	 */
	public void setPrev(Node<T> node) 
	{
		prev = node;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new NodeIterator<T>(this);
	}

	/**
	 * Clase del iterador de Los elementos genericos
	 * @param <K> Elemento generico 
	 */
	private class NodeIterator<K extends Comparable<K>> implements Iterator<K>
	{
		/**
		 * Nodo actual
		 */
		private Node<K> current;

		/**
		 * Iterador del nodo
		 * @param pCurrent Nodo actual
		 */
		private NodeIterator(Node<K> pCurrent) 
		{
			current = pCurrent;
		}

		@Override
		public boolean hasNext() 
		{
			return current!=null;
		}

		@Override
		public K next() 
		{
			K temp = current.elem;
			current = current.next;
			return temp;
		}

	}
}

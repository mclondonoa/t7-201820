package model.data_structures;

import java.util.Iterator;

import model.vo.Bike;


public interface IDoublyLinkedList<T extends Comparable<T>> extends Iterable<T> 
{
	
	public void add(T elem);

	
	public T remove(T elem);

	/**
<<<<<<< HEAD
	 * Tamano de la lista
	 * @return tamano de la lista
=======
	 * Tama�o de la lista
	 * @return tama�o de la lista
>>>>>>> 207925d34bcac50511542ce8640b7db52b656924
	 */
	public int size();

	/**
	 * Obtiene el elemento elem
	 * @param elem elemento buscado
	 * @return el elemento si este existe en la lista, null de lo contrario
	 */
	public T get(T elem);
	
	/**
	 * Obtiene el ultimo elemento 
	 * @param elem ultimo elemento 
	 * @return el elemento si este existe en la lista, null de lo contrario
	 */
	public Node<T> getLast();

	/**
	 * Obtiene el elemento que se encuentra en la posicion pos
	 * @param pos posicion del elemento buscado
	 * @return Elemento en la posicion pos, null si la posicion esta fuera del rago
	 */
	public T get(int pos);
	
	/**
	 * Define si la lista esta vacia
	 * @return True si la lista esta vacia, false de lo contrario
	 */
	public boolean isEmpty();

    @Override
    public Iterator<T> iterator();
    /**
	 * Da el primer nodo de la lista
	 * @return El primer nodo de la lista
	 */
	public Node<T> getFirst();

	public void addFirst(T vo);

	public Node<T> deleteFirst();


}
